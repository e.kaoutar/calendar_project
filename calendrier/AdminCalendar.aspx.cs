﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace calendrier
{

    public partial class AdminCalendar : System.Web.UI.Page
    {

        public SqlConnection myCon;
        private SqlCommand commande;
        private SqlDataReader reader;

        protected void Page_Load(object sender, EventArgs e)
        {
            lblPseudoAdm.Text = Session["username"].ToString();
            openConnection();
            string LoginUser = getReservationsUser(lblPseudoAdm.Text);
            string InfosUser = getReservationsInfos(LoginUser);
            string infosjours = getReservations(LoginUser);
            Response.Output.Write(InfosUser + infosjours);
        }


        public void openConnection()
        {
            myCon = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\asus\Desktop\formatif asp\calendrier\calendrier\App_Data\reserve.mdf;Integrated Security=True");
            myCon.Open();

        }

        public string getReservationsUser(String log)
        {
            //string reservationinfos = "";
            string reserv = "";
            try
            {
                commande = new SqlCommand("select * from Reservation where IdLogin in (select Id from Connexion where Login=@lo);", myCon);
                commande.Parameters.AddWithValue("lo", log);
                reader = commande.ExecuteReader();

                while (reader.Read())
                {
                    reserv = reader["IdLogin"].ToString();
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            reader.Close();
            return reserv;
        }

        public string getReservations(String log)
        {
            //string reservationinfos = "";
            string cnt = "";

            try
            {
                commande = new SqlCommand("select * from Reservation where IdLogin=@lo;", myCon);
                commande.Parameters.AddWithValue("lo", log);
                reader = commande.ExecuteReader();

                while (reader.Read())
                {
                    cnt += reader["Plage"].ToString() + "*";
                    cnt += reader["Jour"].ToString() + "*";

                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            reader.Close();
            return cnt;
        }

        public string getReservationsInfos(string idUser)
        {
            string reservinfos = "";
            int id = Convert.ToInt32(idUser);
            try
            {
                commande = new SqlCommand("select * from Connexion where Id=@i;", myCon);
                commande.Parameters.AddWithValue("i", idUser);
                reader = commande.ExecuteReader();

                while (reader.Read())
                {
                    reservinfos += reader["Nom"].ToString() + ";";
                    reservinfos += reader["Prenom"].ToString() + ";";
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
            reader.Close();
            return reservinfos;
        }

    }


}