﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace calendrier
{
    public class LoginData
    {
        public string Pseudo { get; set; }
        public string Password { get; set; }
        public string Statut { get; set; }
    }
}