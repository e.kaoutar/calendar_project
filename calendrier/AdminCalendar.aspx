﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="AdminCalendar.aspx.cs" Inherits="calendrier.AdminCalendar" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>My Calendar</title>
    <script src="lib/jquery/jquery.min.js"></script>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />

    <link href="StyleLogin.css" rel="stylesheet" />
    <script src="reservation.js"></script>
    <script src="getCalendar.js"></script>
</head>
<body>
    <form id="form1" runat="server">
        <div id="navId">
            <nav class="navbar navbar-light bg-light">
                <span class="navbar-brand mb-0 h1">Calendrier de l'administrateur :
                    <asp:Label ID="lblPseudoAdm" runat="server" Text="Label"></asp:Label></span>


            </nav>
        </div>

        <div id="showData"></div>

        <button type="button" class="btn btn-secondary" data-toggle="modal" data-target="#exampleModal">Faire une réservation</button>

        <!-- Modal -->
        <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title" id="exampleModalLabel">Veuillez saisir les champs pour faire une reservation</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                    <div class="modal-body">
                        <select class="custom-select mr-sm-2" id="inlineFormCustomSelect">
                            <option selected>Plages</option>
                            <option value="1">8h à 9h</option>
                            <option value="2">9h à 10h</option>
                            <option value="3">10h à 11h</option>
                            <option value="3">11h à 12h</option>
                            <option value="3">12h à 13h</option>
                            <option value="3">13h à 14h</option>
                            <option value="3">14h à 15h</option>
                            <option value="3">15h à 16h</option>
                            <option value="3">16h à 17h</option>
                        </select>
                        <br />
                        <select class="custom-select mr-sm-2" id="exampleFormControlSelect1">
                            <option selected>Jours</option>
                            <option value="1">Lundi</option>
                            <option value="2">Mardi</option>
                            <option value="3">Mercredi</option>
                            <option value="3">Jeudi</option>
                            <option value="3">Vendredi</option>
                        </select>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary" id="reserve">Réserver</button>
                    </div>
                </div>
            </div>
        </div>

        <button type="button" class="btn btn-secondary" id="supprime">Supprimer une réservation</button>
        <button type="button" class="btn btn-secondary" id="modif">Modifier une réservation</button>
    </form>


    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js" integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1" crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>
</body>
</html>
