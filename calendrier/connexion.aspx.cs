﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace calendrier
{
    public partial class connexion : System.Web.UI.Page
    {
        //definition des parametres de connexion
        public SqlConnection myCon;
        private SqlCommand commande;
        private SqlDataReader reader;

        //initialisation de l'objet login

        LoginData Ld = new LoginData
        {
            Pseudo = "",
            Password = "",
            Statut = ""
        };

        protected void Page_Load(object sender, EventArgs e)
        {
            //recuperation des inputs du login
            string ps = Request["Username"].ToString();
            string pw = Request["Password"].ToString();
            string st = Request["State"].ToString();

            openConnection();
            Ld = loginFunction(ps, pw, st);

            Session["username"] = ps;

            var json = new JavaScriptSerializer().Serialize(Ld);
            Response.Write(json);

            myCon.Close();
        }

        public void openConnection()
        {
            myCon = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=C:\Users\asus\Desktop\formatif asp\calendrier\calendrier\App_Data\reserve.mdf;Integrated Security=True");

            myCon.Open();

        }

        public LoginData loginFunction(String username, String password, string state)
        {

            try
            {
                commande = new SqlCommand("select * from Connexion where Login=@p and Password=@pw and Statut=@s;", myCon);
                commande.Parameters.AddWithValue("p", username);
                commande.Parameters.AddWithValue("pw", password);
                commande.Parameters.AddWithValue("s", state);
                reader = commande.ExecuteReader();

                while (reader.Read())
                {
                    Ld = new LoginData
                    {
                        Pseudo = reader["Login"].ToString(),
                        Password = reader["Password"].ToString(),
                        Statut = reader["Statut"].ToString()
                    };
                }

            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

            return Ld;
        }
    }
}