﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="pageLogin.aspx.cs" Inherits="calendrier.pageLogin" %>

<!DOCTYPE html>

<html xmlns="http://www.w3.org/1999/xhtml">
<head runat="server">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <script src="lib/jquery/jquery.min.js"></script>
    <title>Bienvenue</title>
    <script src="dataauthentification.js"></script>
    <link href="StyleLogin.css" rel="stylesheet" />
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous" />

</head>
<body>
    <form id="form1" runat="server">
        <div class="wrapper">
            <div class="form-signin">
                <h2 class="form-signin-heading">Connectez vous!</h2>
                <br />
                <br />
                <input type="text" class="form-control" name="username" placeholder="Login" required="" autofocus="" id="username" /><br />
                <input type="password" class="form-control" name="password" placeholder="Password" required="" id="password" />
                <select class="selectpicker" id="statut">
                    <option>Administration</option>
                    <option>Client</option>
                </select>
                <br />
                <br />
                <button type="button" class="btn btn-light" id="logIN">Login</button>
            </div>
        </div>
    </form>
</body>
</html>
