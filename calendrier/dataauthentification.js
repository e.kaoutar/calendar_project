﻿
$(document).ready(function () {
    $("#logIN").click(function () {
        //alert("clique");
        var us = $("#username").val();
        var pw = $("#password").val();
        var st = $("#statut option:selected").val();

        $.get("connexion.aspx?Username=" + us + "&Password=" + pw + "&State=" + st, function (data) {

            var infos = JSON.parse(data);

            if (infos.Pseudo == us && infos.Password == pw) {
                if (infos.Statut == "Administration") {
                    window.open("AdminCalendar.aspx");
                }
                else {
                    window.open("ClientCalendar.aspx");
                }

            }
            else {
                $("#username").css({ "border-color": "red" });
                $("#password").css({ "border-color": "red" });
                $("#username").val("");
                $("#password").val("");
            }
        });
        });
});